<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\NewEmailController;


Route::view('/user_mail_view','email.mail-view');
Route::post('/send-mail',[NewEmailController::class,'sendEmail']);