<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmailController;


Route::post('/email',[EmailController::class,'sendEmail']);
Route::view('/mail_view','email.send-mail-form');