<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class EmailController extends Controller
{
    
    public function sendEmail(Request $req){
        $user=$req->input();

        // dd($user);
        Mail::send('mail',$user,function($messages) use ($user){
            $messages->to($user['reciver_email']);
            $messages->subject($user['subject']);
        });
        return back()->with('success','Email send !');
    }
}

