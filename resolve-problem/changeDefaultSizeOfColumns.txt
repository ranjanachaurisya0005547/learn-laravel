NOTE:
If your system not support the default size(225) of laravel then in this case 
you need to change the default size of laravel.
For That You need to do this:

Step 1:
Go to App/Providers\AppServiceProviders.php

Step 2)
App The below namespace inside the AppServiceProviders.php file

Use Illuminate\Support\Facades\Schema;

Step 3)Add the below function inside the register method of AppServiceProviders.php file
Like:

public function register(){
        Schema::defaultStringLength(191);
}