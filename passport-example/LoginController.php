<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Validator;
use App\Models\User;

class LoginController extends Controller
{
    //Create user
    public function registration(Request $request){
        $rule=[
          'name'=>'required|String',
          'email'=>'required|email',
          'mobile'=>'required|regex:/^[6-9]{1}[0-9]{9}$/',
          'password'=>'required|min:8',
          'confirm_password'=>'required|same:password'
        ];

        $validator=Validator::make($request->all(),$rule);

        if($validator->fails()){
              return response()->json($validator->errors(),201);
        }else{

            try{
                  $allData=$request->input();
                     $token=str::random(60);          
                  $user=User::create([
                        "name"=>$allData['name'],
                        "email"=>$allData['email'],
                        "mobile"=>$allData['mobile'],
                        "password"=>\Hash::make($allData['password']),
                        "remember_token"=>$token,
                  ]);

                  return response()->json(["data"=>$user,"success"=>true,"status"=>"200"],400);

            }catch(\Exception $ex){
                return response()->json(['message'=>$ex->getMessage()],202);
            }               
              
        }
    }

    //Login Authroized user
    public function login(Request $request){
        $rules=[
            'email' => 'required|string',
            'password' => 'required|string'
        ];

        $validator=Validator::make($request->all(),$rules);

        if($validator->fails()){
             return response()->json(['errors'=>$validator->errors(),'success'=>false,'status'=>201]);
        }else{
             try{
                 $user=User::where('email',$request->email)->first();
                 if($user){
                        if(Hash::check($request->password,$user->password)){
                            $token=$user->createToken('accessToken')->accessToken;
                            return response()->json(['message'=>"Login Successfully !",'success'=>true,'status'=>200,'token'=>$token]);
                        }else{
                            return response()->json(['message'=>'Invalid Password !','success'=>false,'status'=>203]);
                        }
                 }else{
                    return response()->json(['message'=>'Invalid Email Address !','success'=>false,'status'=>202]);
                 }

             }catch(\Exception $ex){
                 return response()->json(['message'=>$ex->getMessage(),'success'=>false,'status'=>404]);
             }
        }
    }


    //logout
    public function logout(Request $request){
        try{
             $token=$request->user()->token();
             $token->revoke();
             return response()->json(['message'=>"Logout Successfully !",'success'=>true,'status'=>200]);
        }catch(\Exception $ex){
            return response()->json(['errors'=>$ex->getMessage(),'success'=>false,'status'=>405]);
        }
    }
}
