Razorpay Payment Gateway Integration in Laravel 8 Tutorial:
Step 1: Install Laravel:
cmd)composer create-project --prefer-dist laravel/laravel blog

Step 2: Create Razorpay Account:
First you need to create account on razorpay. then you can easily get account key id and key secret.
Create Account from here: www.razorpay.com.
After register successfully. you need to go bellow link and get id and secret as bellow screen shot:
Go Here: https://dashboard.razorpay.com/app/keys.

##Next you can get account key id and secret and add on .env file as like bellow:
fileName).env

RAZORPAY_KEY=rzp_test_XXXXXXXXX
RAZORPAY_SECRET=XXXXXXXXXXXXXXXX

Step 3: Install razorpay/razorpay Package:
In this step, we need to install razorpay/razorpay composer package to use razorpay api. 
so let's run bellow command:
cmd)composer require razorpay/razorpay

Step 4: Create Route:
<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RazorpayPaymentController;

//Payment Gateway Integration
Route::get('/razorpay-gateway',[RazorpayPaymentController::class,'showPaymentForm'])->name('payment.gateway');
Route::post('razorpay-payment',[RazorpayPaymentController::class,'makePayment'])->name('razorpay.payment.store');
Route::post('/payment_success',[RazorpayPaymentController::class,'paySuccess'])->name('razorpay.success');


================
Step 5: Create Controller:
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Payment;
use Razorpay\Api\Api;
use Session;

class RazorpayPaymentController extends Controller
{
    //payment form
    public function showPaymentForm(){
        return view('paymentGateway.paymentView');
    }

    //payment store
    public function makePayment(Request $request){
        $name=$request->input('name');
        $amount=$request->input('amount');

        $api_key=env('RAZOREPAY_KEY');
        $api_secret=env('RAZORPAY_SECRET');

        $api=new Api($api_key,$api_secret);
        $order  = $api->order->create(array('receipt' => '123', 'amount' => $amount * 100, 'currency' => 'INR')); 
        $orderId=$order['id'];

         $product=Payment::create([
               'name'=>$name,
               'amount'=>$amount,
               'payment_id'=>$orderId,
         ]);

         $product->save();

        Session::put('order_id',$orderId);
        Session::put('amount',$amount);

       return redirect('/razorpay-gateway');
        

    }

    public function paySuccess(Request $request){
        $data=$request->all();
        //dd($data);
        $user=Payment::where('payment_id',$data['razorpay_order_id'])->first();
        $user->payment_done=true;
        $user->razorpay_id=$data['razorpay_payment_id'];
        $user->save();

        return view('paymentGateway.successpayment');
    }
}

=====================
Step 6: Create Blade File:
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Laravel - Payment Gateway Using Razorpay...</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

</head>
<body>
<div class="container">

    <!-------------Paymnet Submission Form----------------------->
    <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-6">
            <div class="card mt-5">
                <div class="card-header">
                    Payment With Razorpay...
                </div>
                <div class="card-body">
                    <form action="{{route('razorpay.payment.store')}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label>Enter Your Name</label>
                            <input type="text" name="name" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label>Enter Your Amout</label>
                            <input type="text" name="amount" class="form-control"/>
                        </div>
                      <input type="submit" class="btn btn-primary"/>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-3"></div>
    </div>

    <!------------------Razorpay submission Form-------------->
    @if(Session::has('amount'))
    <div class="row">
        <form action="{{ route('razorpay.success') }}" method="POST" >
                                    @csrf
                                    <script src="https://checkout.razorpay.com/v1/checkout.js"
                                            data-key="{{ env('RAZOREPAY_KEY') }}"
                                            data-amount="{{Session::get('amount')}}"
                                            data-currency="INR"
                                            data-buttontext="Pay with Razorpay"
                                            data-name="pizza"
                                            data-order_id="{{Session::get('order_id')}}"
                                            data-description="Rozerpay"
                                            data-prefill.name="name"
                                            data-prefill.email="email"
                                            data-theme.color="#ff7529"
                                    >
                                    </script>
                                 <input type="hidden" custom="Hidden Element" name="hidden">
                                </form>
    </div>
    @endif
</div>
</body>
</html>

========

##Note:Success Blade File
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Payment Successful......</title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

	<style>
		.content{
			margin-top:10%;
			color:green;
			font-style:italic;
			font-family:calibri;
		}
	</style>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6 content">
			<h2>Payment Done Successfully</h2><br/>
			<a href="{{route('payment.gateway')}}" class="btn btn-primary btn-lg mr-5">Back</a>
		</div>
		<div class="col-sm-3"></div>
	</div>
</div>
</body>
</html>

====================
Migration:
 public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->string('name',50);
            $table->string('amount',100);
            $table->string('payment_id')->nullable();
            $table->string('razorpay_id')->nullable();
            $table->string('payment_done')->default(false);
            $table->timestamps();
        });
    }
===
cmd)php artisan migrate
cmd)php artisan serve	


