<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\LoginController;



//User Login using api
Route::prefix('user')->group(function(){
    Route::post('/registration',[LoginController::class,'registration']);
    Route::post('/login',[LoginController::class,'login']);
});

Route::prefix('/admin')->group(function(){
    Route::middleware('auth:api')->group(function(){
        Route::view('/dashboard','api.admin.dashboard');
        Route::get('/logout',[LoginController::class,'logout']);
    });
});